"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let prefixV1 = "/v1";
exports.uri = {
    // Base APIs
    BASE: {
        // CASM
        CASM: {
            createCustomer: prefixV1 + "/casm/customers/create",
            updateCustomerCustomName: prefixV1 + "/casm/customers/update-custom-name",
            updateCustomerExternalId: prefixV1 + "/casm/customers/update-external-id",
            updateCustomerSegment: prefixV1 + "/casm/customers/update-segment",
            updateCustomerState: prefixV1 + "/casm/customers/update-state",
            createAccount: prefixV1 + "/casm/accounts/create",
            updateAccountBillCycle: prefixV1 + "/casm/accounts/update-bill-cycle",
            updateAccountCustomName: prefixV1 + "/casm/accounts/update-custom-name",
            updateAccountExternalId: prefixV1 + "/casm/accounts/update-external-id",
            updateAccountSegment: prefixV1 + "/casm/accounts/update-segment",
            updateAccountState: prefixV1 + "/casm/accounts/update-state",
            createSubscriber: prefixV1 + "/casm/subscribers/create",
            updateSubscriberCustomName: prefixV1 + "/casm/subscribers/update-custom-name",
            updateSubscriberExternalId: prefixV1 + "/casm/subscribers/update-external-id",
            updateSubscriberSegment: prefixV1 + "/casm/subscribers/update-segment",
            updateSubscriberState: prefixV1 + "/casm/subscribers/update-state",
            createSubscription: prefixV1 + "/casm/subscriptions/create",
            updateSubscriptionCustomName: prefixV1 + "/casm/subscriptions/update-custom-name",
            updateSubscriptionExternalId: prefixV1 + "/casm/subscriptions/update-external-id",
            updateSubscriptionState: prefixV1 + "/casm/subscriptions/update-state",
        },
        CHARGING: {
            // Charging
            mobileSms: prefixV1 + "/charging/telco/3gpp/sms-iec",
            mobileVoice: prefixV1 + "/charging/telco/3gpp/voice-iec",
            registerFile: prefixV1 + "/charging/batch-request-loader/register-file",
            closeFile: prefixV1 + "/charging/batch-request-loader/close-file"
        },
    },
    // Model APIs
    MODEL: {
        CORE: {
            billCycle: prefixV1 + "/bill-cycles",
            resourceType: prefixV1 + "/resource-types"
        },
        TELCO: {
            destination: prefixV1 + "/telco/destinations",
            interconnectGroup: prefixV1 + "/telco/interconnect-groups",
            interconnectPoint: prefixV1 + "/telco/interconnect-points"
        }
    }
};
