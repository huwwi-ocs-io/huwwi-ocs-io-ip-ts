export declare const uri: {
    BASE: {
        CASM: {
            createCustomer: string;
            updateCustomerCustomName: string;
            updateCustomerExternalId: string;
            updateCustomerSegment: string;
            updateCustomerState: string;
            createAccount: string;
            updateAccountBillCycle: string;
            updateAccountCustomName: string;
            updateAccountExternalId: string;
            updateAccountSegment: string;
            updateAccountState: string;
            createSubscriber: string;
            updateSubscriberCustomName: string;
            updateSubscriberExternalId: string;
            updateSubscriberSegment: string;
            updateSubscriberState: string;
            createSubscription: string;
            updateSubscriptionCustomName: string;
            updateSubscriptionExternalId: string;
            updateSubscriptionState: string;
        };
        CHARGING: {
            mobileSms: string;
            mobileVoice: string;
            registerFile: string;
            closeFile: string;
        };
    };
    MODEL: {
        CORE: {
            billCycle: string;
            resourceType: string;
        };
        TELCO: {
            destination: string;
            interconnectGroup: string;
            interconnectPoint: string;
        };
    };
};
