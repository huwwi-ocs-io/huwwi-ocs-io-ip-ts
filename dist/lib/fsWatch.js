"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var watch = require('watch');
class FsWatch {
    constructor(dir, options) {
        watch.createMonitor(dir, options, (monitor) => {
            monitor.on('created', (file, stat) => {
                this.onCreated(file, stat);
            });
            monitor.on('changed', (file, curr, prev) => {
                this.onChanged(file, curr, prev);
            });
            monitor.on('removed', (file, stat) => {
                this.onRemoved(file, stat);
            });
        });
    }
}
exports.FsWatch = FsWatch;
;
