export declare class CsvRecord {
    private data;
    constructor(data: Array<any>);
    getNumber(idx: number): number | null;
    getString(idx: number): string | null;
    getDate(idx: number, pattern: string, timezone?: string | null): Date | null;
    getData(): Array<String>;
}
export declare abstract class CsvParser {
    abstract onData(rec: CsvRecord, context: any): any;
    private context;
    private parser;
    constructor(csvData: any, isStream?: boolean, options?: any);
    parse(context?: any): Promise<any>;
}
