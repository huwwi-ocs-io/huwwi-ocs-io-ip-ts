"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require('moment-timezone');
var csv = require('fast-csv');
var _ = require('lodash');
class CsvRecord {
    constructor(data) {
        this.data = data;
    }
    getNumber(idx) {
        let attr = this.data[idx];
        if (_.isNil(attr) || attr == "") {
            return null;
        }
        let num = parseInt(attr);
        if (!_.isNaN(num)) {
            return num;
        }
        else {
            throw new Error("Parameter '" + attr + "' is not a valid number.");
        }
    }
    getString(idx) {
        let attr = this.data[idx];
        if (_.isNil(attr) || attr == "") {
            return null;
        }
        return attr;
    }
    getDate(idx, pattern, timezone = null) {
        let attr = this.data[idx];
        if (_.isNil(attr) || attr == "") {
            return null;
        }
        if (_.isNil(timezone)) {
            return moment(pattern).toDate();
        }
        else {
            return moment.tz(attr, pattern, timezone).toDate();
        }
    }
    getData() {
        return this.data;
    }
}
exports.CsvRecord = CsvRecord;
class CsvParser {
    constructor(csvData, isStream = false, options = {}) {
        if (isStream) {
            this.parser = csv.parse.fromStream(csvData, options);
        }
        else {
            this.parser = csv.parse.fromString(csvData, options);
        }
    }
    parse(context = undefined) {
        return __awaiter(this, void 0, void 0, function* () {
            this.context = context;
            let self = this;
            let cnt = 0;
            return yield new Promise((resolve, reject) => {
                this.parser
                    .on('data', function (rec) {
                    return __awaiter(this, void 0, void 0, function* () {
                        cnt++;
                        self.context = self.onData(new CsvRecord(rec), self.context);
                    });
                })
                    .on('end', function () {
                    resolve(self.context);
                });
            });
        });
    }
}
exports.CsvParser = CsvParser;
