export declare abstract class FsWatch {
    constructor(dir: string, options: any);
    abstract onCreated(file: any, stat: any): Promise<void>;
    abstract onChanged(file: any, curr: any, prev: any): Promise<void>;
    abstract onRemoved(file: any, stat: any): Promise<void>;
}
