import { BaseApi } from '../framework/baseApi';
import { ChargingRequest, ChargingResponse } from '../ocs/rbsTypes';
import * as telco from './telcoTypes';
export declare class TelcoApi extends BaseApi {
    allUsage(path: string, req: ChargingRequest): Promise<ChargingResponse>;
    mobileSms(req: telco.MobileSmsRequest): Promise<ChargingResponse>;
}
