"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ocsURI_1 = require("../config/ocsURI");
const modelApi_1 = require("../framework/modelApi");
class TelcoModelApi extends modelApi_1.ModelApi {
    destinationsAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.queryAll(ocsURI_1.uri.MODEL.TELCO.destination);
        });
    }
    interconnectGroupsAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.queryAll(ocsURI_1.uri.MODEL.TELCO.interconnectGroup);
        });
    }
    interconnectPointsAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.queryAll(ocsURI_1.uri.MODEL.TELCO.interconnectPoint);
        });
    }
}
exports.TelcoModelApi = TelcoModelApi;
