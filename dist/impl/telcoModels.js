"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DestinationType;
(function (DestinationType) {
    DestinationType[DestinationType["COUNTRY"] = 0] = "COUNTRY";
    DestinationType[DestinationType["NUMBERING_PLAN"] = 1] = "NUMBERING_PLAN";
    DestinationType[DestinationType["MOBILE"] = 2] = "MOBILE";
    DestinationType[DestinationType["FIXED"] = 3] = "FIXED";
})(DestinationType = exports.DestinationType || (exports.DestinationType = {}));
;
var UsageAnalysisType;
(function (UsageAnalysisType) {
    UsageAnalysisType[UsageAnalysisType["BEST_MATCH"] = 0] = "BEST_MATCH";
    UsageAnalysisType[UsageAnalysisType["EXACT_MATCH"] = 1] = "EXACT_MATCH";
    UsageAnalysisType[UsageAnalysisType["REGEXP"] = 2] = "REGEXP";
})(UsageAnalysisType = exports.UsageAnalysisType || (exports.UsageAnalysisType = {}));
;
;
