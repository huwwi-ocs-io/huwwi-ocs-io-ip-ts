import { ModelApi } from '../framework/modelApi';
import { QueryModelResponse } from '../framework/modelTypes';
import { Destination, InterconnectGroup, InterconnectPoint } from '../impl/telcoModelTypes';
export declare class TelcoModelApi extends ModelApi {
    destinationsAll(): Promise<QueryModelResponse<Destination>>;
    interconnectGroupsAll(): Promise<QueryModelResponse<InterconnectGroup>>;
    interconnectPointsAll(): Promise<QueryModelResponse<InterconnectPoint>>;
}
