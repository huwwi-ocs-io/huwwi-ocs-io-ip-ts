import { EntityCodeModel } from '../framework/modelTypes';
export declare enum DestinationType {
    COUNTRY = 0,
    NUMBERING_PLAN = 1,
    MOBILE = 2,
    FIXED = 3,
}
export declare enum UsageAnalysisType {
    BEST_MATCH = 0,
    EXACT_MATCH = 1,
    REGEXP = 2,
}
export interface DestinationModel extends EntityCodeModel {
    countryRefId?: string;
    destinationType: DestinationType;
    plmn?: string;
    mcc?: string;
    mnc?: string;
    destinationAnalysisType?: UsageAnalysisType;
    value?: string;
    priority?: number;
    parentRefId?: string;
}
