"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ocsURI_1 = require("../config/ocsURI");
const baseApi_1 = require("../framework/baseApi");
class TelcoApi extends baseApi_1.BaseApi {
    allUsage(path, req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(path, req);
        });
    }
    mobileSms(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CHARGING.mobileSms, req);
        });
    }
}
exports.TelcoApi = TelcoApi;
;
