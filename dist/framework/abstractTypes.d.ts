export interface ValidationViolation {
    path?: string;
    code?: string;
    message?: string;
    parameters?: Map<String, String>;
}
export interface Result {
    status?: string;
    code?: string;
    message?: string;
    parameters?: string;
    violations: Set<ValidationViolation>;
}
export interface AbstractRequest {
}
export interface AbstractResponse {
    result: Result;
}
