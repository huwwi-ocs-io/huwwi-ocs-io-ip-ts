import { AbstractApi } from './abstractApi';
import { ApiRequest, ApiResponse, ApiRequests, ApiResponses } from './baseTypes';
export declare class BaseApi extends AbstractApi {
    get(path: string): Promise<ApiResponse>;
    post(path: string, payload: ApiRequest): Promise<ApiResponse>;
    bulkPost(reqs: ApiRequests, sequential?: boolean): Promise<ApiResponses>;
    bulkPostSync(reqs: ApiRequests, callback: (rsps: ApiResponses, ctx?: any) => void, ctx?: any): void;
}
