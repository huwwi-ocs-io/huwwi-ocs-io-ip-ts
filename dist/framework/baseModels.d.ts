import { Result } from './baseTypes';
export interface Model {
    refId: string;
}
export interface EntityModel extends Model {
    name: string;
    description?: string;
}
export interface EntityCodeModel extends EntityModel {
    internalCode: string;
}
export interface RecordSetModel<T extends Model> {
    result: Result;
    total: number;
    data: Array<T>;
}
