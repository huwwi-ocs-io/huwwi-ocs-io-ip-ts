import { AbstractApi } from './abstractApi';
import { Model, QueryModelResponse } from './modelTypes';
export declare class ModelApi extends AbstractApi {
    queryAll<T extends Model>(path: string): Promise<QueryModelResponse<T>>;
    getByRefId<T extends Model>(path: string, refId: string): Promise<T>;
    static queryToMap<T extends Model>(data: Array<T>): Map<string, T>;
}
