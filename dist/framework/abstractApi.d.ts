import { AbstractResponse } from './abstractTypes';
export declare abstract class AbstractApi {
    private restClient;
    private endpoint;
    private appId;
    private apiKey;
    private pathPrefix;
    constructor(endpoint: string, appId: string, apiKey: string);
    protected getPathPrefix(): string | undefined;
    private getRestClient();
    protected doGet(path: string): Promise<any>;
    protected doPost(path: string, payload: any): Promise<any>;
    private createError(obj);
    getError(obj: AbstractResponse): Error | undefined;
}
