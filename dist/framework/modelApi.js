"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const abstractApi_1 = require("./abstractApi");
var _ = require('lodash');
class ModelApi extends abstractApi_1.AbstractApi {
    queryAll(path) {
        const _super = name => super[name];
        return __awaiter(this, void 0, void 0, function* () {
            return _super("doGet").call(this, path);
        });
    }
    getByRefId(path, refId) {
        const _super = name => super[name];
        return __awaiter(this, void 0, void 0, function* () {
            if (!_.isNil(refId)) {
                path += "/" + refId;
            }
            return _super("doGet").call(this, path);
        });
    }
    static queryToMap(data) {
        let map = new Map();
        for (let mdl of data) {
            map.set(mdl.refId, mdl);
        }
        return map;
    }
}
exports.ModelApi = ModelApi;
