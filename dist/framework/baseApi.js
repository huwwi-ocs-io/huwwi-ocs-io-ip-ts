"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const abstractApi_1 = require("./abstractApi");
class BaseApi extends abstractApi_1.AbstractApi {
    get(path) {
        const _super = name => super[name];
        return __awaiter(this, void 0, void 0, function* () {
            return _super("doGet").call(this, path);
        });
    }
    post(path, payload) {
        const _super = name => super[name];
        return __awaiter(this, void 0, void 0, function* () {
            return _super("doPost").call(this, path, payload);
        });
    }
    bulkPost(reqs, sequential) {
        return __awaiter(this, void 0, void 0, function* () {
            let promises = new Array();
            for (let req of reqs) {
                promises.push(this.doPost(req._uri, req));
            }
            return Promise.all(promises);
        });
    }
    bulkPostSync(reqs, callback, ctx) {
        let promises = new Array();
        for (let req of reqs) {
            promises.push(this.doPost(req._uri, req));
        }
        Promise.all(promises).then((rsps) => {
            callback(rsps, ctx);
        });
    }
}
exports.BaseApi = BaseApi;
