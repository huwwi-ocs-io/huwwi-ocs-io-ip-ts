import { AbstractRequest, AbstractResponse } from './abstractTypes';
export interface ApiRequest extends AbstractRequest {
    requestId?: string;
    requestDate?: Date;
    transactionId?: string;
    _uri: string;
    _context?: any;
}
export declare class ApiRequests extends Array<ApiRequest> {
}
export interface ApiResponse extends AbstractResponse {
    name: string;
    requestId?: string;
    seqno: number;
    object: object;
    objectType: string;
    _uri?: string;
    _context?: any;
}
export interface ApiBulkResponse extends AbstractResponse {
    name: string;
    requestId?: string;
    data?: Array<ApiResponse>;
}
export declare class ApiResponses extends Array<ApiResponse> {
}
