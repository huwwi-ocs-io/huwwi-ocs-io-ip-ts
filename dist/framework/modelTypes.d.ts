import { AbstractResponse } from './abstractTypes';
export interface Model {
    refId: string;
}
export interface EntityModel extends Model {
    name: string;
    description?: string;
}
export interface EntityCodeModel extends EntityModel {
    internalCode: string;
}
export interface QueryModelResponse<T extends Model> extends AbstractResponse {
    total: number;
    data: Array<T>;
}
