import { ModelApi } from '../framework/modelApi';
import { QueryModelResponse } from '../framework/modelTypes';
import { BillCycle, ResourceType } from '../ocs/coreModelTypes';
export declare class CoreModelApi extends ModelApi {
    billCyclesAll(): Promise<QueryModelResponse<BillCycle>>;
    resourceTypesAll(): Promise<QueryModelResponse<ResourceType>>;
}
