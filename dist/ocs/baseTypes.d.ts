export interface ApiRequest {
    requestId?: string;
    requestDate?: Date;
    transactionId?: string;
    _uri: string;
    _context?: any;
}
export declare class ApiRequests extends Array<ApiRequest> {
}
export interface ValidationViolation {
    path?: string;
    code?: string;
    message?: string;
    parameters?: Map<String, String>;
}
export interface Result {
    status?: string;
    code?: string;
    message?: string;
    parameters?: string;
    violations: Set<ValidationViolation>;
}
export interface ApiResponse {
    result: Result;
    name: string;
    requestId?: string;
    seqno: number;
    object: object;
    objectType: string;
    _uri?: string;
    _context?: any;
}
export interface ApiBulkResponse {
    result: Result;
    name: string;
    requestId?: string;
    data?: Array<ApiResponse>;
}
export declare class ApiResponses extends Array<ApiResponse> {
}
