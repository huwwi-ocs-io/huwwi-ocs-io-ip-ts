import * as rm from 'typed-rest-client/RestClient';
import { ApiRequest, ApiResponse, ApiRequests, ApiResponses } from './baseTypes';
export declare abstract class AbstractApi {
    private restClient;
    private endpoint;
    private appId;
    private apiKey;
    private pathPrefix;
    constructor(endpoint: string, appId: string, apiKey: string);
    protected getRestClient(): rm.RestClient;
    doGet(path: string): Promise<ApiResponse>;
    doPost(path: string, payload: ApiRequest): Promise<ApiResponse>;
    bulkPost(reqs: ApiRequests, sequential?: boolean): Promise<ApiResponses>;
    bulkPostSync(reqs: ApiRequests, callback: (rsps: ApiResponses, ctx?: any) => void, ctx?: any): void;
    private getError(obj);
    protected decodeOcsResponse(err: any, obj: ApiResponse, dontLogAlreadyCharged?: boolean): Error;
}
