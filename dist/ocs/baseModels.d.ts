export interface Model {
    refId: string;
}
export interface EntityModel extends Model {
    name: string;
    description?: string;
}
export interface EntityCodeModel extends EntityModel {
    internalCode: string;
}
