import * as core from '../framework/baseTypes';
export interface StateType {
    state: string;
    stateReason?: string;
    stateValidFrom?: Date;
}
export interface IdType {
    refId?: string;
    externalId?: string;
}
export interface CreateCustomerRequest extends core.ApiRequest {
    customerProfile?: string;
    customerExternalId?: string;
    customerSegment?: string;
    state?: StateType;
    testCustomer?: Boolean;
    customName?: string;
    customAttributes?: Map<string, string>;
}
export interface CreateAccountRequest extends core.ApiRequest {
    accountProfile?: string;
    accountExternalId?: string;
    accountType?: string;
    customer?: IdType;
    accountSegment?: string;
    billCycle?: string;
    currency?: string;
    state?: StateType;
    paymentResponsible?: Boolean;
    testAccount?: Boolean;
    parent?: IdType;
    customName?: string;
    customAttributes?: Map<string, string>;
}
export interface CreateSubscriberRequest extends core.ApiRequest {
    subscriberProfile?: string;
    subscriberExternalId?: string;
    account?: IdType;
    primaryResourceType: string;
    primaryResource: string;
    subscriberSegment?: string;
    customName?: string;
    customAttributes?: Map<string, string>;
}
export interface CreateSubscriptionRequest extends core.ApiRequest {
    customer?: IdType;
    customerSegment?: string;
    account?: IdType;
    accountSegment?: string;
    subscriber?: IdType;
    subscriberSegment?: string;
    offer: string;
    subscriptionExternalId?: string;
    state?: StateType;
    customName?: string;
    customAttributes?: Map<string, string>;
}
export interface UpdateAccountBillCycleRequest extends core.ApiRequest {
    account: IdType;
    billCycle: string;
    validFrom: Date;
}
export interface UpdateAccountCustomNameRequest extends core.ApiRequest {
    account: IdType;
    accountCustomName?: string;
}
export interface UpdateAccountExternalIdRequest extends core.ApiRequest {
    account: IdType;
    accountExternalId?: string;
}
export interface UpdateAccountSegmentRequest extends core.ApiRequest {
    account: IdType;
    accountSegment?: string;
}
export interface UpdateAccountStateRequest extends core.ApiRequest {
    account: IdType;
    accountState: StateType;
}
export interface UpdateCustomerCustomNameRequest extends core.ApiRequest {
    customer: IdType;
    customerCustomName?: string;
}
export interface UpdateCustomerExternalIdRequest extends core.ApiRequest {
    customer: IdType;
    customerExternalId?: string;
}
export interface UpdateCustomerSegmentRequest extends core.ApiRequest {
    customer: IdType;
    customerSegment?: string;
}
export interface UpdateCustomerStateRequest extends core.ApiRequest {
    customer: IdType;
    customerState: StateType;
}
export interface UpdateSubscriberCustomNameRequest extends core.ApiRequest {
    subscriber: IdType;
    subscriberCustomName?: string;
}
export interface UpdateSubscriberExternalIdRequest extends core.ApiRequest {
    subscriber: IdType;
    subscriberExternalId?: string;
}
export interface UpdateSubscriberStateRequest extends core.ApiRequest {
    subscriber: IdType;
    subscriberState: StateType;
}
export interface UpdateSubscriberSegmentRequest extends core.ApiRequest {
    subscriber: IdType;
    subscriberSegment?: string;
}
export interface UpdateSubscriptionCustomNameRequest extends core.ApiRequest {
    subscription: IdType;
    subscriptionCustomName?: string;
}
export interface UpdateSubscriptionExternalIdRequest extends core.ApiRequest {
    subscription: IdType;
    subscriptionExternalId?: string;
}
export interface UpdateSubscriptionStateRequest extends core.ApiRequest {
    subscription: IdType;
    subscriptionState: StateType;
}
