"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const rm = require("typed-rest-client/RestClient");
var urljoin = require('url-join');
var _ = require('lodash');
class AbstractApi {
    constructor(endpoint, appId, apiKey) {
        this.endpoint = endpoint;
        this.appId = appId;
        this.apiKey = apiKey;
        // assign path prefix
        var idxProtocol = endpoint.indexOf("//");
        var idxPath = endpoint.indexOf("/", (idxProtocol < 0 ? 0 : idxProtocol + 2));
        if (idxPath < 0) {
            this.pathPrefix = undefined;
        }
        else {
            this.pathPrefix = endpoint.substr(idxPath);
        }
    }
    getRestClient() {
        if (this.restClient == null) {
            this.restClient = new rm.RestClient("ocs", this.endpoint, undefined, { ignoreSslError: true });
        }
        return this.restClient;
    }
    doGet(path) {
        return __awaiter(this, void 0, void 0, function* () {
            let hres = yield this.getRestClient().get(this.pathPrefix == undefined ? path : urljoin(this.pathPrefix, path), { additionalHeaders: {
                    'Content-Type': 'application/json',
                    'X-Huwwi-Application-Id': this.appId,
                    'X-Huwwi-API-Key': this.apiKey
                }
            });
            return hres.result;
        });
    }
    doPost(path, payload) {
        return __awaiter(this, void 0, void 0, function* () {
            let hres = yield this.getRestClient().create(this.pathPrefix == undefined ? path : urljoin(this.pathPrefix, path), payload, { additionalHeaders: {
                    'Content-Type': 'application/json',
                    'X-Huwwi-Application-Id': this.appId,
                    'X-Huwwi-API-Key': this.apiKey
                }
            });
            hres.result._context = payload._context;
            return hres.result;
        });
    }
    bulkPost(reqs, sequential) {
        return __awaiter(this, void 0, void 0, function* () {
            let promises = new Array();
            for (let req of reqs) {
                promises.push(this.doPost(req._uri, req));
            }
            return Promise.all(promises);
        });
    }
    bulkPostSync(reqs, callback, ctx) {
        let promises = new Array();
        for (let req of reqs) {
            promises.push(this.doPost(req._uri, req));
        }
        Promise.all(promises).then((rsps) => {
            callback(rsps, ctx);
        });
    }
    getError(obj) {
        var error = new Error();
        return _.merge(error, obj);
    }
    decodeOcsResponse(err, obj, dontLogAlreadyCharged) {
        let error;
        if (!obj) {
            error = this.getError({
                message: 'Missing OCS response',
                response: obj
            });
        }
        else if (!obj.result) {
            error = this.getError({
                message: 'Not valid OCS response content',
                response: obj
            });
        }
        else if (!obj.result.status) {
            error = this.getError({
                message: 'Missing status in OCS response content',
                response: obj
            });
        }
        else if (obj.result.status !== 'OK') {
            error = this.getError({
                message: obj.result.message,
                code: obj.result.code,
                violations: obj.result.violations,
                status: obj.result.status,
            });
        }
        else {
            if (err)
                console.warn(err);
            else
                console.log(obj);
            return err;
        }
        return error;
    }
}
exports.AbstractApi = AbstractApi;
