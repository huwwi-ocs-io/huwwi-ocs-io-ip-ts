import { BaseApi } from '../framework/baseApi';
import * as rbs from './rbsTypes';
export declare class RbsApi extends BaseApi {
    registerFile(req: rbs.RegisterFileRequest): Promise<void>;
}
