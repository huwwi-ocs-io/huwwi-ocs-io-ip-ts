"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ocsURI_1 = require("../config/ocsURI");
const baseApi_1 = require("../framework/baseApi");
class CasmApi extends baseApi_1.BaseApi {
    createCustomer(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.createCustomer, req);
        });
    }
    createAccount(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.createAccount, req);
        });
    }
    createSubscriber(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.createSubscriber, req);
        });
    }
    createSubscription(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.createSubscription, req);
        });
    }
    updateCustomerCustomName(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateCustomerCustomName, req);
        });
    }
    updateCustomerExternalId(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateCustomerExternalId, req);
        });
    }
    updateCustomerSegment(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateCustomerSegment, req);
        });
    }
    updateCustomerState(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateCustomerState, req);
        });
    }
    updateAccountCustomName(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateAccountCustomName, req);
        });
    }
    updateAccountBillCycle(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateAccountBillCycle, req);
        });
    }
    updateAccountExternalId(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateAccountExternalId, req);
        });
    }
    updateAccountSegment(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateAccountSegment, req);
        });
    }
    updateAccountState(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateAccountState, req);
        });
    }
    updateSubscriberCustomName(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateSubscriberCustomName, req);
        });
    }
    updateSubscriberExternalId(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateSubscriberExternalId, req);
        });
    }
    updateSubscriberSegment(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateSubscriberSegment, req);
        });
    }
    updateSubscriberState(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateSubscriberState, req);
        });
    }
    updateSubscriptionCustomName(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateSubscriptionCustomName, req);
        });
    }
    updateSubscriptionExternalId(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateSubscriptionExternalId, req);
        });
    }
    updateSubscriptionState(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.post(ocsURI_1.uri.BASE.CASM.updateSubscriptionState, req);
        });
    }
}
exports.CasmApi = CasmApi;
