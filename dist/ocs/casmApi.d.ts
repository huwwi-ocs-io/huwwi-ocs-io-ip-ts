import { BaseApi } from '../framework/baseApi';
import { ApiBulkResponse } from '../framework/baseTypes';
import * as casm from './casmTypes';
export declare class CasmApi extends BaseApi {
    createCustomer(req: casm.CreateCustomerRequest): Promise<ApiBulkResponse>;
    createAccount(req: casm.CreateAccountRequest): Promise<ApiBulkResponse>;
    createSubscriber(req: casm.CreateSubscriberRequest): Promise<ApiBulkResponse>;
    createSubscription(req: casm.CreateSubscriptionRequest): Promise<ApiBulkResponse>;
    updateCustomerCustomName(req: casm.UpdateCustomerCustomNameRequest): Promise<ApiBulkResponse>;
    updateCustomerExternalId(req: casm.UpdateCustomerExternalIdRequest): Promise<ApiBulkResponse>;
    updateCustomerSegment(req: casm.UpdateCustomerSegmentRequest): Promise<ApiBulkResponse>;
    updateCustomerState(req: casm.UpdateCustomerStateRequest): Promise<ApiBulkResponse>;
    updateAccountCustomName(req: casm.UpdateAccountCustomNameRequest): Promise<ApiBulkResponse>;
    updateAccountBillCycle(req: casm.UpdateAccountBillCycleRequest): Promise<ApiBulkResponse>;
    updateAccountExternalId(req: casm.UpdateAccountExternalIdRequest): Promise<ApiBulkResponse>;
    updateAccountSegment(req: casm.UpdateAccountSegmentRequest): Promise<ApiBulkResponse>;
    updateAccountState(req: casm.UpdateAccountStateRequest): Promise<ApiBulkResponse>;
    updateSubscriberCustomName(req: casm.UpdateSubscriberCustomNameRequest): Promise<ApiBulkResponse>;
    updateSubscriberExternalId(req: casm.UpdateSubscriberExternalIdRequest): Promise<ApiBulkResponse>;
    updateSubscriberSegment(req: casm.UpdateSubscriberSegmentRequest): Promise<ApiBulkResponse>;
    updateSubscriberState(req: casm.UpdateSubscriberStateRequest): Promise<ApiBulkResponse>;
    updateSubscriptionCustomName(req: casm.UpdateSubscriptionCustomNameRequest): Promise<ApiBulkResponse>;
    updateSubscriptionExternalId(req: casm.UpdateSubscriptionExternalIdRequest): Promise<ApiBulkResponse>;
    updateSubscriptionState(req: casm.UpdateSubscriptionStateRequest): Promise<ApiBulkResponse>;
}
