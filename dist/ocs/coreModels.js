"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TimeUnit;
(function (TimeUnit) {
    TimeUnit[TimeUnit["SECOND"] = 0] = "SECOND";
    TimeUnit[TimeUnit["MINUTE"] = 1] = "MINUTE";
    TimeUnit[TimeUnit["HOUR"] = 2] = "HOUR";
    TimeUnit[TimeUnit["DAY"] = 3] = "DAY";
    TimeUnit[TimeUnit["WEEK"] = 4] = "WEEK";
    TimeUnit[TimeUnit["MONTH"] = 5] = "MONTH";
    TimeUnit[TimeUnit["YEAR"] = 6] = "YEAR";
})(TimeUnit = exports.TimeUnit || (exports.TimeUnit = {}));
;
;
