import { EntityCodeModel } from '../framework/baseModels';
export declare enum TimeUnit {
    SECOND = 0,
    MINUTE = 1,
    HOUR = 2,
    DAY = 3,
    WEEK = 4,
    MONTH = 5,
    YEAR = 6,
}
export interface BillCycle extends EntityCodeModel {
    duration: number;
    billingPeriod: TimeUnit;
    billingDay: number;
    automaticBilling?: boolean;
    billingOffset?: number;
}
