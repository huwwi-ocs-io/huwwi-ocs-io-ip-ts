import { AbstractApi } from '../framework/baseApi';
import { ApiResponse } from '../framework/baseTypes';
export declare class CoreApi extends AbstractApi {
    billCycles(): Promise<ApiResponse>;
}
