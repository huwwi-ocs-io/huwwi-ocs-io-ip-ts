import {uri} from '../config/ocsURI';

import {BaseApi} from '../framework/baseApi'
import {ApiResponse} from '../framework/baseTypes'
import {ChargingRequest, ChargingResponse} from '../ocs/rbsTypes'
import * as telco from './telcoTypes'


export class TelcoApi extends BaseApi {
  
  public async allUsage(path: string, req: ChargingRequest): Promise<ChargingResponse> {
    return <ChargingResponse> await this.post(path, req);
  }

  
  public async mobileSms(req: telco.MobileSmsRequest): Promise<ChargingResponse> {
    return <ChargingResponse> await this.post(uri.BASE.CHARGING.mobileSms, req);
  }

  // public mobileVoice(req: MobileVoiceRequest) {
  //   this.doPost(config.ocs.PATH.mobileVoice, req, function(err: any, rsp: ApiResponse) {
  //     let myRsp = <ChargingResponse> rsp;
  //   });
  // }

};
