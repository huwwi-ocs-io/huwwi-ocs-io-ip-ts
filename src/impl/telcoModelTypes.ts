import { Model, EntityCodeModel } from '../framework/modelTypes';

export enum DestinationType {
    COUNTRY,
    NUMBERING_PLAN,
    MOBILE,
    FIXED
};

export enum UsageAnalysisType {

    BEST_MATCH
    , EXACT_MATCH
    , REGEXP

};

export interface Destination extends EntityCodeModel {
    countryRefId?: string,
    destinationType: DestinationType,
    plmn?: string,
    mcc?: string,
    mnc?: string,
    destinationAnalysisType?: UsageAnalysisType,
    value?: string,
    priority?: number,
    parentRefId?: string
};

export interface InterconnectGroup extends EntityCodeModel {
    resourceTypeRefId: string,
    resourceValue: string
};

export interface InterconnectPoint extends Model {
    interconnectGroupRefId: string,
    code: string,
    validDrom?: Date,
    validTo?: Date
};