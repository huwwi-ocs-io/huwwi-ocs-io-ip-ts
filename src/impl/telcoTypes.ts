import { ChargingRequest } from '../ocs/rbsTypes'
import { EntityCodeModel } from '../framework/modelTypes';

export interface MobileSmsRequest extends ChargingRequest {
  recordType: string,
  recordingEntity?: string,

  vs3GPPmscMcc?: string,
  vs3GPPmscMnc?: string,
  servedImsi?: string,
  servedImei?: string,
  servedMsisdn?: string,
  originatorImsi?: string,
  originatorMsisdn?: string,
  originatorMsisdnTon?: string,
  originatorOtherAddress?: string,
  originatorReceivedAddress?: string,
  recipientImsi?: string,
  recipientMsisdn?: string,
  recipientMsisdnTon?: string,
  recipientOtherAddress?: string,
  recipientReceivedAddress?: string,
  submissionTime?: Date,
  eventTimestamp: Date,
  smPriority?: number,
  messageReference: string,
  smTotalNumber?: number,
  smSequenceNumber?: number,
  messageSize?: number,
  smDeliveryReportRequested?: boolean,
  smStatus?: string,
  userLocationInfo?: string,
  ueTimeZone?: string,
  smsResult?: string
};

export interface MobileVoiceRequest extends ChargingRequest {
  recordType: string,
  servedImsi: string,
  servedImei?: string,
  servedMsisdn: string,
  callingNumber: string,
  callingNumberTon: string,
  calledNumber: string,
  calledNumberTon: string,
  dialledNumber?: string,
  dialledNumberTon?: string,
  translatedNumber?: string,
  translatedNumberTon?: string,
  connectedNumber?: string,
  connectedNumberTon?: string,
  roamingNumber?: string,
  recordingEntity?: string,
  vs3GPPmscMcc?: string,
  vs3GPPmscMnc?: string,
  location?: string,
  basicServiceType: number,
  basicServiceCode: number,
  answerTime: Date,
  callDuration: number,
  causeForTerm?: number,
  callReference: string,
  sequenceNumber?: number,
  serviceKey?: number,
  networkCallReference: string,
  mSCAddress?: string,
  partialRecordType?: number,
  originalCalledNumber?: string,
  recordNumber?: number
};



