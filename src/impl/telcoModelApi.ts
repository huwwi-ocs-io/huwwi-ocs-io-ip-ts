import {uri} from '../config/ocsURI';

import {ModelApi} from '../framework/modelApi'

import {QueryModelResponse} from '../framework/modelTypes'
import {Destination, InterconnectGroup, InterconnectPoint} from '../impl/telcoModelTypes'


export class TelcoModelApi extends ModelApi {

  public async destinationsAll(): Promise<QueryModelResponse<Destination>> {
    return <QueryModelResponse<Destination>> await this.queryAll(uri.MODEL.TELCO.destination);
  }

  public async interconnectGroupsAll(): Promise<QueryModelResponse<InterconnectGroup>> {
    return <QueryModelResponse<InterconnectGroup>> await this.queryAll(uri.MODEL.TELCO.interconnectGroup);
  }

  public async interconnectPointsAll(): Promise<QueryModelResponse<InterconnectPoint>> {
    return <QueryModelResponse<InterconnectPoint>> await this.queryAll(uri.MODEL.TELCO.interconnectPoint);
  }

  
}