import { AbstractApi } from './abstractApi'

import {Model, QueryModelResponse} from './modelTypes'

var _ = require('lodash')

export class ModelApi extends AbstractApi {

	public async queryAll<T extends Model>(
		path: string
	): Promise<QueryModelResponse<T>> {

    	return super.doGet(path);
		
	}

	public async getByRefId<T extends Model>(
		path: string,
		refId: string
	): Promise<T> {

		if( ! _.isNil(refId) ) {
			path += "/" + refId;
		}

    	return super.doGet(path);
		
	}

	public static queryToMap<T extends Model>(data: Array<T>): Map<string, T> {

		let map: Map<string, T> = new Map<string, T>();

		for( let mdl of data ) {
			map.set(mdl.refId, mdl);
		}

		return map;
	}

  
}