import * as rm from 'typed-rest-client/RestClient';

import {AbstractRequest, AbstractResponse} from './abstractTypes';

var urljoin = require('url-join');

var _ = require('lodash');

export abstract class AbstractApi {

	private restClient: rm.RestClient; // singleton
	
	// REST API parameters
	private endpoint: string;
	private appId: string;
	private apiKey: string;
	private pathPrefix: string | undefined;

	constructor(
		endpoint: string,
		appId: string,
		apiKey: string
		) {
			this.endpoint = endpoint;
			this.appId = appId;
			this.apiKey = apiKey;

			// assign path prefix
			var idxProtocol = endpoint.indexOf("://");
			var idxPath = endpoint.indexOf("/", (idxProtocol < 0 ? 0 : idxProtocol+3))
			
			if( idxPath < 0 ) {
				this.pathPrefix = undefined;
			}
			else {
				this.pathPrefix = endpoint.substr(idxPath);
			}
		}

		protected getPathPrefix(): string | undefined {
			return this.pathPrefix;
		}
	
		private getRestClient() {
		if (this.restClient == null) {
			this.restClient = new rm.RestClient("ocs", this.endpoint, undefined, { ignoreSslError: true });
		}
		return this.restClient;

	}

	protected async doGet(
		path: string
	): Promise<any> {

		let hres: rm.IRestResponse<any> = await this.getRestClient().get<any>(
			this.pathPrefix == undefined ? path : urljoin(this.pathPrefix, path),
			{ additionalHeaders: {
					'Content-Type': 'application/json',
					'X-Huwwi-Application-Id': this.appId,
					'X-Huwwi-API-Key': this.apiKey
				}
			}
		);

		return hres.result;

	}


	protected async doPost(
		path: string,
		payload: any
	): Promise<any> {

		let hres: rm.IRestResponse<any> = await this.getRestClient().create<any>(
			this.pathPrefix == undefined ? path : urljoin(this.pathPrefix, path),
			payload,
			{ additionalHeaders: {
					'Content-Type': 'application/json',
					'X-Huwwi-Application-Id': this.appId,
					'X-Huwwi-API-Key': this.apiKey
				}
			}
		);

		hres.result._context = payload._context;

		return hres.result;

	}

	private createError(obj: any): Error {
		var error = new Error();
		return _.merge(error, obj);
	}


	public getError(obj: AbstractResponse): Error | undefined {
		let error: Error | undefined = undefined;

		if (!obj) {
			error = this.createError({
				message: 'Missing OCS response',
				response: obj
			});
		} else if (!obj.result) {
			error = this.createError({
				message: 'Not valid OCS response content',
				response: obj
			});
		} else if (!obj.result.status) {
			error = this.createError({
				message: 'Missing status in OCS response content',
				response: obj
			});
		} else if (obj.result.status !== 'OK') {
			error = this.createError({
				message: obj.result.message,
				code: obj.result.code,
				violations: obj.result.violations,
				status: obj.result.status,
			});
		}

		return error;
	}


}
