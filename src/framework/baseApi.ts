import {uri} from '../config/ocsURI';

import { AbstractApi } from './abstractApi'

import * as rm from 'typed-rest-client/RestClient';

import { ApiRequest, ApiResponse, ApiRequests, ApiResponses } from './baseTypes';

export class BaseApi extends AbstractApi {

	
	public async get(
		path: string
	): Promise<ApiResponse> {


		return super.doGet(path);

	}

	public async post(
		path: string,
		payload: ApiRequest
	): Promise<ApiResponse> {

		return super.doPost(path, payload);

	}

	public async bulkPost(reqs: ApiRequests, sequential?: boolean): Promise<ApiResponses> {

		let promises: Array<Promise<ApiResponse>> = new Array<Promise<ApiResponse>>();

		for (let req of reqs) {
			promises.push(this.doPost(req._uri, req));
		}

		return Promise.all(promises);
	}


	public bulkPostSync(reqs: ApiRequests, callback: (rsps: ApiResponses, ctx?: any) => void, ctx?: any) {

		let promises: Array<Promise<ApiResponse>> = new Array<Promise<ApiResponse>>();

		for (let req of reqs) {
			promises.push(this.doPost(req._uri, req));
		}

		Promise.all(promises).then((rsps: ApiResponses) => {
			callback(rsps, ctx);
		});
	}


}

