var moment = require('moment-timezone')


var csv = require('fast-csv');

var  _ = require('lodash');

export class CsvRecord {

  private data: Array<any>;

  constructor(data: Array<any>) {
    this.data = data;
  }

  public getNumber(idx: number): number | null {

    let attr: any = this.data[idx];

    if( _.isNil(attr) || attr == "" ) {
      return null;
    }

    let num: number = parseInt(attr);
    if( ! _.isNaN(num) ) {
      return num;
    }
    else {
      throw new Error("Parameter '" + attr + "' is not a valid number.");
    }

  }
  
  public getString(idx: number): string | null {

    let attr: any = this.data[idx];

    if( _.isNil(attr) || attr == "" ) {
      return null;
    }

    return attr;
  }

  public getDate(idx: number, pattern: string, timezone: string | null = null): Date | null {

    let attr: any = this.data[idx];

    if( _.isNil(attr) || attr == "" ) {
      return null;
    }

    if( _.isNil(timezone) ) {
      return moment(pattern).toDate();
    }
    else {
      return moment.tz(attr, pattern, timezone).toDate();
    }
  }

  public getData(): Array<String> {
    return this.data;
  }
  
  
}

export abstract class CsvParser {

  public abstract onData(rec: CsvRecord, context: any): any;

  private context:any;

  private parser: any;

  constructor(csvData: any, isStream: boolean = false, options: any = {}) {

    if (isStream ) {
      this.parser = csv.parse.fromStream(csvData, options);
    }
    else {
      this.parser = csv.parse.fromString(csvData, options);
    }

  }

  public async parse(context: any = undefined): Promise<any> {

    this.context = context;

    let self = this;

    let cnt: number = 0;

    return await new Promise<any>( (resolve, reject) => {
      this.parser
        .on('data', async function (rec: Array<any>) {
          cnt++;

          self.context = self.onData(new CsvRecord(rec), self.context);

        })
        .on('end', function() {
          resolve(self.context);
        });
      });
    }

}