var watch = require('watch');

export abstract class FsWatch {

  constructor(dir: string, options: any) {

    watch.createMonitor(dir, options, (monitor: any) => {
      monitor.on('created', (file: any, stat: any) => {
        
        this.onCreated(file, stat);
    
      });
    
      monitor.on('changed', (file: any, curr: any, prev: any) => {
    
        this.onChanged(file, curr, prev);
    
      });
    
      monitor.on('removed', (file: any, stat: any) => {
    
        this.onRemoved(file, stat);
    
      });
    });

  }

  abstract async onCreated(file: any, stat: any): Promise<void>;
  abstract onChanged(file: any, curr: any, prev: any): Promise<void>;
  abstract onRemoved(file: any, stat: any): Promise<void>;

};

