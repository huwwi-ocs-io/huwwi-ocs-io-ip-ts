import {uri} from '../config/ocsURI';


import {BaseApi} from '../framework/baseApi'
import {ApiBulkResponse} from '../framework/baseTypes'
import * as casm from './casmTypes'

export class CasmApi extends BaseApi {
  
  public async createCustomer(req: casm.CreateCustomerRequest): Promise<ApiBulkResponse> {
      return await this.post(uri.BASE.CASM.createCustomer, req);
  }

  public async createAccount(req: casm.CreateAccountRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.createAccount, req);
  }

  public async createSubscriber(req: casm.CreateSubscriberRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.createSubscriber, req);
  }

  public async createSubscription(req: casm.CreateSubscriptionRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.createSubscription, req);
  }

  public async updateCustomerCustomName(req: casm.UpdateCustomerCustomNameRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateCustomerCustomName, req);
  }

  public async updateCustomerExternalId(req: casm.UpdateCustomerExternalIdRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateCustomerExternalId, req);
  }

  public async updateCustomerSegment(req: casm.UpdateCustomerSegmentRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateCustomerSegment, req);
  }

  public async updateCustomerState(req: casm.UpdateCustomerStateRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateCustomerState, req);
  }

  public async updateAccountCustomName(req: casm.UpdateAccountCustomNameRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateAccountCustomName, req);
  }

  public async updateAccountBillCycle(req: casm.UpdateAccountBillCycleRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateAccountBillCycle, req);
  }

  public async updateAccountExternalId(req: casm.UpdateAccountExternalIdRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateAccountExternalId, req);
  }

  public async updateAccountSegment(req: casm.UpdateAccountSegmentRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateAccountSegment, req);
  }

  public async updateAccountState(req: casm.UpdateAccountStateRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateAccountState, req);
  }

  public async updateSubscriberCustomName(req: casm.UpdateSubscriberCustomNameRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateSubscriberCustomName, req);
  }

  public async updateSubscriberExternalId(req: casm.UpdateSubscriberExternalIdRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateSubscriberExternalId, req);
  }

  public async updateSubscriberSegment(req: casm.UpdateSubscriberSegmentRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateSubscriberSegment, req);
  }

  public async updateSubscriberState(req: casm.UpdateSubscriberStateRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateSubscriberState, req);
  }

  public async updateSubscriptionCustomName(req: casm.UpdateSubscriptionCustomNameRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateSubscriptionCustomName, req);
  }

  public async updateSubscriptionExternalId(req: casm.UpdateSubscriptionExternalIdRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateSubscriptionExternalId, req);
  }

  public async updateSubscriptionState(req: casm.UpdateSubscriptionStateRequest): Promise<ApiBulkResponse> {
    return await this.post(uri.BASE.CASM.updateSubscriptionState, req);
  }

  

}

