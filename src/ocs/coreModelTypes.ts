import {EntityCodeModel} from '../framework/modelTypes';

export enum TimeUnit {

  SECOND
, MINUTE
, HOUR
, DAY
, WEEK
, MONTH
, YEAR

};

export interface BillCycle extends EntityCodeModel {
  duration: number,
  billingPeriod: TimeUnit,
  billingDay: number,
  automaticBilling?: boolean,
  billingOffset?: number
};

export interface ResourceType extends EntityCodeModel {
  seqno: number,
  primary?: boolean
};