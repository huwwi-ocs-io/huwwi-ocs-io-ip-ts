import {uri} from '../config/ocsURI';

import {ModelApi} from '../framework/modelApi'
import {QueryModelResponse} from '../framework/modelTypes'
import {BillCycle, ResourceType} from '../ocs/coreModelTypes'

export class CoreModelApi extends ModelApi {
  
  public async billCyclesAll(): Promise<QueryModelResponse<BillCycle>> {
    return <QueryModelResponse<BillCycle>> await this.queryAll(uri.MODEL.CORE.billCycle);
  }

  public async resourceTypesAll(): Promise<QueryModelResponse<ResourceType>> {
    return <QueryModelResponse<ResourceType>> await this.queryAll(uri.MODEL.CORE.resourceType);
  }

}
