import * as core from '../framework/baseTypes'

export interface ChargingRequest extends core.ApiRequest {
  usageType: string,
  fileRefId?: string
};

export interface ChargingResponse extends core.ApiResponse {
  rejectedEventStored?: boolean,
  eventTotalVolume: number,
  eventTotalPrice?: number,
  eventTotalPriceNet?: number,
  eventTotalPriceTax?: number,
  eventInvoicedPrice?: number,
  currency: string
  // impactedBalances: Array<any>
};

export interface RegisterFileRequest extends core.ApiRequest {
  loader: string,
  fileName: string,
  fullName: string,
  hash?: string,
  fileCreatedDate: Date,
  temporaryName?: string
};

export interface BatchRequestStatType {
  usageType: string,
  resultStatus: string,
  resultCode?: string,
  eventCount: number
};

export interface CloseFileRequest extends core.ApiRequest {
  fileRefId: string,
  eventCount: number,
  rejectedCount: number,
  // statistics?: Array<BatchRequestStatType>
};

