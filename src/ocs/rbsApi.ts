import {uri} from '../config/ocsURI';


import {BaseApi} from '../framework/baseApi'
import {ApiResponse} from '../framework/baseTypes'
import * as rbs from './rbsTypes'

export class RbsApi extends BaseApi {
  
  public async registerFile(req: rbs.RegisterFileRequest) {
      let rsp: ApiResponse = <ApiResponse> await this.post(uri.BASE.CHARGING.registerFile, req);
  }

  // public async closeFile(req: CloseFileRequest) {
  //   this.doPost(config.ocs.PATH.closeFile, req, function(err: any, rsp: ApiResponse) {
  //     let myRsp = <ApiResponse> rsp;
  //     if( err ) {
  //       console.log("Error, msg=" + err.message + ", code=" + err.code + ", status=" + err.status);
  //     }
  //     else {
  //       if( myRsp.object ) {
  //         let obj: Model = <Model> myRsp.object;
  //         console.log("OK, refId=" + obj.refId);
  //       }
  //     }
  //   });
  // }


};

